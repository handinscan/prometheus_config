#!/bin/bash

#!IMPORTANT
#Create db user mysqld_exporter / mUFewSh6hQHmkX9r
echo "IMPORTANT CREATE SQL USER:"
echo "CREATE USER 'mysqld_exporter'@'localhost' IDENTIFIED BY 'mUFewSh6hQHmkX9r' WITH MAX_USER_CONNECTIONS 3;"
echo "GRANT PROCESS, REPLICATION CLIENT, SELECT ON *.* TO 'mysqld_exporter'@'localhost';"

set -x

echo "Start MySQLd Exporter install..."
cd ~

version="0.11.0"

echo "Download"
curl -LO "https://github.com/prometheus/mysqld_exporter/releases/download/v"$version"/mysqld_exporter-"$version".linux-amd64.tar.gz"

tar xvf "mysqld_exporter-"$version".linux-amd64.tar.gz"

sudo useradd --no-create-home --shell /bin/false mysqld_exporter
sudo mv "mysqld_exporter-"$version".linux-amd64/mysqld_exporter" /usr/local/bin/

sudo chown mysqld_exporter:mysqld_exporter /usr/local/bin/mysqld_exporter

rm -rf "mysqld_exporter-"$version".linux-amd64.tar.gz" "mysqld_exporter-"$version".linux-amd64/"

sudo mkdir /etc/mysqld_exporter

sudo chown mysqld_exporter:mysqld_exporter /etc/mysqld_exporter

echo "Writing config files"
cat <<EOF >./.my.cnf
[client]
user=mysqld_exporter
password=mUFewSh6hQHmkX9r
EOF
sudo mv ./.my.cnf /etc/mysqld_exporter/.my.cnf


cat <<EOF >./mysqld_exporter.service
[Unit]
Description=Prometheus MySQL Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=mysqld_exporter
Group=mysqld_exporter
Type=simple
WorkingDirectory=/etc/mysqld_exporter/
ExecStart=/usr/local/bin/mysqld_exporter \
    --config.my-cnf="/etc/mysqld_exporter/.my.cnf"
[Install]
WantedBy=multi-user.target
EOF
sudo mv ./mysqld_exporter.service /etc/systemd/system/mysqld_exporter.service

sudo chown -R  mysqld_exporter:mysqld_exporter /etc/mysqld_exporter/

sudo systemctl daemon-reload
sudo systemctl enable mysqld_exporter
sudo systemctl start mysqld_exporter.service

echo "IMPORTANT - MODIFY PROMETHEUS CONFIGURATION"
