#!/bin/bash
set -x

echo "Start Node Exporter install..."
cd ~

#get latest version from here: https://github.com/prometheus/node_exporter/releases
version="0.16.0"


echo "Download"
curl -LO "https://github.com/prometheus/node_exporter/releases/download/v"$version"/node_exporter-"$version".linux-amd64.tar.gz"
echo "Unpack"
tar xvf "node_exporter-"$version".linux-amd64.tar.gz"

echo "Install"
sudo useradd --no-create-home --shell /bin/false node_exporter
sudo cp "node_exporter-"$version".linux-amd64/node_exporter" /usr/local/bin
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter

rm -rf "node_exporter-"$version".linux-amd64.tar.gz" "node_exporter-"$version".linux-amd64"

echo "Write service config"
sudo cat <<EOF >./node_exporter.service
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
EOF
sudo mv ./node_exporter.service /etc/systemd/system/node_exporter.service

echo "Start service and Node exporter"
sudo systemctl daemon-reload
sudo systemctl start node_exporter
sudo systemctl status node_exporter
sudo systemctl enable node_exporter

echo "IMPORTANT - MODIFY PROMETHEUS CONFIGURATION"
